package main

import (
    "fmt"
    "math"
)

func main(){
    var x, y float64
    fmt.Printf("Enter x: ")
    fmt.Scan(&x)
    fmt.Printf("Enter y: ")
    fmt.Scan(&y)

    fmt.Println("The hypotnuse is:", math.Sqrt(math.Pow(x,2)+math.Pow(y,2)))
}
