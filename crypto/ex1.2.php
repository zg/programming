<?php
$cipher = "xultpaajcxitltlxaarpjhtiwtgxktghidhipxciwtvgtpilpitghlxiwiwtxgqadds";

$tot = strlen($cipher);

$freq = array_flip(range('a','z'));

foreach(str_split($cipher) as $c)
    $freq[$c]++;

arsort($freq);
foreach($freq as $c => $cnt)
    printf("%s=%3d (%.4f%%)\n",$c,$cnt,$cnt/$tot);
