<?php
function mod($num,$mod){
    return ($mod + ($num % $mod)) % $mod;
}

$a = 7;
$a_inv = 15;
$b = 22;
$ciphertext = "falszztysyjzyjkywjrztyjztyynaryjkyswarztyegyyj";
$alpha = array_flip(range('a','z'));
foreach(str_split($ciphertext) as $c)
    printf("%c",ord('a')+mod($a_inv*($alpha[$c]-$b),26));
printf("\n");
