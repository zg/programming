
# after these letters, write и instead of ы
seven_letter_spelling_rule = ['к','г','х','ш','щ','ж','ч']

# after these letters, write е instead of о
five_letter_spelling_rule = ['ш','щ','ж','ч','ц']

questions = ['кто','что','где','когда','почему','как']

# large, interesting, pretty, small, new, bad, last, old, good
# не + <word> => "not" <word>
adjective_lemmas = ['больш','интересн','красив','маленьк','нов','плох','последн','стар','хорош']
# modifiers of adjectives

# whose, my, your (informal), our, your (formal)
masculine = ['чей','мой','твой','наш','ваш']
neuter = ['чьё','моё','твоё','наше','ваше']
feminine = ['чья','моя','твоя','наша','ваша']
plural = ['чьи','мои','твои','наши','ваши']

for i in x:
    print(i)
