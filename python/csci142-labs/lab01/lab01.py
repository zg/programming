# naive Prime Sieve algorithm
# Zack Gold   zg@mail.rit.edu

def findPrimes(N):
    primes = []
    for n in range(2,N):
        is_prime = True
        for prime in primes:
            if n % prime == 0:
                is_prime = False
        if is_prime:
            primes.append(n)
    return primes

try:
    print(findPrimes(int(input("Enter N: "))))
except ValueError:
    print("Invalid input.")
