from FreqBand import FreqBand
from StationData import StationData

class CarRadio:
    stationData = None
    POWER_OFF = False
    POWER_ON = True
    MUTE_OFF = False
    MUTE_ON = True
    
    MIN_VOLUME = 0
    VOLUME_DELTA = 1
    MAX_VOLUME = 20

    TUNE_TYPE_AM = 0
    TUNE_TYPE_FM = 1
    MIN_AM_STATION = FreqBand.AM.getMinFreq()
    AM_DELTA = FreqBand.AM.getSpacing()
    MAX_AM_STATION = FreqBand.AM.getMaxFreq()
    MIN_FM_STATION = FreqBand.FM.getMinFreq()
    FM_DELTA = FreqBand.FM.getSpacing()
    MAX_FM_STATION = FreqBand.FM.getMaxFreq()

    PRESET_OFF = False
    PRESET_ON = True

    powerState = POWER_OFF
    muteState = MUTE_OFF

    currentVolume = MIN_VOLUME

    tuneType = TUNE_TYPE_AM
    currentFMStation = MIN_FM_STATION
    currentAMStation = MIN_AM_STATION

    presetState = PRESET_OFF
    presets = []

    def __init__(self,stationData):
        self.stationData = stationData

        self.powerState = self.POWER_OFF
        self.muteState = self.MUTE_OFF
        self.currentVolume = self.MIN_VOLUME
        self.tuneType = self.TUNE_TYPE_AM
        self.currentFMStation = self.MIN_FM_STATION
        self.currentAMStation = self.MIN_AM_STATION
        self.presetState = self.PRESET_OFF
        for preset in range(0,10):
            self.presets.append(self.MIN_AM_STATION if preset < 5 else self.MIN_FM_STATION)

    def powerBtn(self):
        if self.presetState == self.PRESET_ON:
            setBtn()
        if self.powerState == self.POWER_OFF:
            self.powerState = self.POWER_ON
        else:
            self.powerState = self.POWER_OFF

    def volumeUpBtn(self):
        if self.powerState == self.POWER_ON:
            if self.currentVolume < self.MAX_VOLUME:
                self.currentVolume += self.VOLUME_DELTA

    def volumeDownBtn(self):
        if self.powerState == self.POWER_ON:
            if self.currentVolume > self.MIN_VOLUME:
                self.currentVolume -= self.VOLUME_DELTA

    def muteBtn(self):
        if self.powerState == self.POWER_ON:
            if self.muteState == self.MUTE_OFF:
                self.muteState = self.MUTE_ON
            elif self.muteState == self.MUTE_ON:
                self.muteState = self.MUTE_OFF

    def amfmBtn(self):
        if self.powerState == self.POWER_ON:
            if self.presetState == self.PRESET_ON:
                setBtn()
            if self.tuneType == self.TUNE_TYPE_AM:
                self.tuneType = self.TUNE_TYPE_FM
            elif self.tuneType == self.TUNE_TYPE_FM:
                self.tuneType = self.TUNE_TYPE_AM

    def tuneUpBtn(self):
        if self.powerState == self.POWER_ON:
            if self.presetState == self.PRESET_ON:
                self.setBtn()
            currentStation = self.getStation(self.tuneType)
            if self.tuneType == self.TUNE_TYPE_AM:
                if currentStation < self.MAX_AM_STATION:
                    self.setStation(self.tuneType,currentStation + self.AM_DELTA)
                elif currentStation == self.MAX_AM_STATION:
                    self.setStation(self.tuneType,self.MIN_AM_STATION)
            elif self.tuneType == self.TUNE_TYPE_FM:
                if currentStation < self.MAX_FM_STATION:
                    self.setStation(self.tuneType, currentStation + self.FM_DELTA)
                elif currentStation == self.MAX_FM_STATION:
                    self.setStation(self.tuneType, self.MIN_FM_STATION)
    
    def tuneDownBtn(self):
        if self.powerState == self.POWER_ON:
            if self.presetState == self.PRESET_ON:
                self.setBtn()
            currentStation = self.getStation(self.tuneType)
            if self.tuneType == self.TUNE_TYPE_AM:
                if currentStation > self.MIN_AM_STATION:
                    self.setStation(self.tuneType, currentStation - self.AM_DELTA)
                elif currentStation == self.MIN_AM_STATION:
                    self.setStation(self.tuneType, self.MAX_AM_STATION)
            elif self.tuneType == self.TUNE_TYPE_FM:
                if currentStation > self.MIN_FM_STATION:
                    self.setStation(self.tuneType, currentStation - self.FM_DELTA)
                elif currentStation == self.MIN_FM_STATION:
                    self.setStation(self.tuneType, self.MAX_FM_STATION)

    def seekUpBtn(self):
        if self.powerState == self.POWER_ON:
            if self.presetState == self.PRESET_ON:
                self.setBtn()
            currentStation = self.getStation(self.tuneType)
            self.tuneUpBtn()
            while self.getStationName(self.tuneType,self.getStation(self.tuneType)) == None and currentStation != self.getStation(self.tuneType):
                self.tuneUpBtn()

    def seekDownBtn(self):
        if self.powerState == self.POWER_ON:
            if self.presetState == self.PRESET_ON:
                self.setBtn()
            currentStation = self.getStation(self.tuneType)
            self.tuneDownBtn()
            while self.getStationName(self.tuneType,self.getStation(self.tuneType)) == None and currentStation != self.getStation(self.tuneType):
                self.tuneDownBtn()

    def setBtn(self):
        if self.powerState == self.POWER_ON:
            if self.presetState == self.PRESET_OFF:
                self.presetState = self.PRESET_ON
            else:
                self.presetState = self.PRESET_OFF

    def presetStation(self,index):
        if self.powerState == self.POWER_ON:
            if self.presetState == self.PRESET_ON:
                self.setPreset(self.tuneType,index,self.getStation(self.tuneType))
                self.setBtn()
            elif self.presetState == self.PRESET_OFF:
                self.setStation(self.tuneType,self.getPreset(self.tuneType,index))

    def preset1Btn(self):
        self.presetStation(0)

    def preset2Btn(self):
        self.presetStation(1)

    def preset3Btn(self):
        self.presetStation(2)

    def preset4Btn(self):
        self.presetStation(3)

    def preset5Btn(self):
        self.presetStation(4)

    def getPreset(self,tuneType,presetIndex):
        try:
            return self.presets[tuneType * presetIndex]
        except IndexError:
            return 0

    def setPreset(self,tuneType,presetIndex,station):
        try:
            self.presets[tuneType * presetIndex] = station
        except IndexError:
            print("Preset doesn't exist!")

    def getStationName(self,tuneType,station):
        try:
            if tuneType == self.TUNE_TYPE_AM:
                return self.stationData.lookupFreq(FreqBand.AM,station)
            elif tuneType == self.TUNE_TYPE_FM:
                return self.stationData.lookupFreq(FreqBand.FM,station)
        except IndexError:
            return None
        return None

    def getStation(self,tuneType):
        if tuneType == self.TUNE_TYPE_AM:
            return self.currentAMStation
        elif tuneType == self.TUNE_TYPE_FM:
            return self.currentFMStation

    def setStation(self,tuneType,station):
        if tuneType == self.TUNE_TYPE_AM:
            self.currentAMStation = station
        elif tuneType == self.TUNE_TYPE_FM:
            self.currentFMStation = station

    def display(self):
        display = []
        dashes = "----------------------"
        line2 = ""
        line3 = ""

        if self.powerState == self.POWER_OFF:
            line2 = "|                    |"
            line3 = line2
        elif self.powerState == self.POWER_ON:
            stationInt = self.getStation(self.tuneType)
            stationStr = str(stationInt)
            stationName = self.getStationName(self.tuneType,stationInt)
            if stationName == None:
                stationName = "****"
            stationDisplay = ""
            if self.tuneType == self.TUNE_TYPE_AM:
                stationDisplay = stationStr
            elif self.tuneType == self.TUNE_TYPE_FM:
                stationDisplay = stationStr[0:stationStr.__len__()-3]+"."+stationStr[stationStr.__len__()-3]
            line2 = "|  " + ("AM" if self.tuneType == self.TUNE_TYPE_AM else "FM") + (" " if stationDisplay.__len__() == 3 else "") + (" " if stationDisplay.__len__() == 5 else "  ") + " " + stationDisplay + "   " + stationName + "  |"
            line3 = "|  Vol:  "
            if self.muteState == self.MUTE_ON:
                line3 += "--"
            elif self.muteState == self.MUTE_OFF:
                line3 += str(self.currentVolume) + (" " if self.currentVolume < 10 else "")
            line3 += "    " + ("SET" if self.presetState == self.PRESET_ON else "   ") + "   |"
        display.append(dashes)
        display.append(line2)
        display.append(line3)
        display.append(dashes)
        return display
