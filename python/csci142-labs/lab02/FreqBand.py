from enum import Enum

class FreqBand(Enum):
    AM = (520,1610,10)
    FM = (87900,107900,200)

    def __init__(self, minFreq, maxFreq, spacing):
        self.minFreq = minFreq
        self.maxFreq = maxFreq
        self.spacing = spacing

    def getMinFreq(self):
        return self.minFreq

    def getMaxFreq(self):
        return self.maxFreq

    def getSpacing(self):
        return self.spacing
