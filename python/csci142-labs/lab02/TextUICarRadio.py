from FreqBand import FreqBand
from StationData import StationData
from CarRadio import CarRadio

class TextUICarRadio:
    def main():
        comment = False
        sd = None
        while sd is None:
            print("Valid locations are: BostonMA DeathValleyCA NewYorkNY RochesterNY")
            loc = input("Location: ")
            if loc == 'BostonMA':
                sd = StationData().mkBostonMA()
            elif loc == 'DeathValleyCA':
                sd = StationData().mkDeathValleyCA()
            elif loc == 'NewYorkNY':
                sd = StationData().mkNewYorkNY()
            elif loc == 'RochesterNY':
                sd = StationData().mkRochesterNY()
            else:
                print("Unknown location: " + loc)
        radio = CarRadio(sd)
        quit = False
        while quit is False:
            if comment is False:
                for line in radio.display():
                    print(line)
            else:
                comment = False
            cmd = input("command: ")
            if cmd == 'quit':
                quit = True
            elif cmd == 'power':
                radio.powerBtn()
            elif cmd == 'volumeUp':
                radio.volumeUpBtn()
            elif cmd == 'volumeDown':
                radio.volumeDownBtn()
            elif cmd == 'mute':
                radio.muteBtn()
            elif cmd == 'amfm':
                radio.amfmBtn()
            elif cmd == 'tuneUp':
                radio.tuneUpBtn()
            elif cmd == 'tuneDown':
                radio.tuneDownBtn()
            elif cmd == 'seekUp':
                radio.seekUpBtn()
            elif cmd == 'seekDown':
                radio.seekDownBtn()
            elif cmd == 'set':
                radio.setBtn()
            elif cmd == 'preset1':
                radio.preset1Btn()
            elif cmd == 'preset2':
                radio.preset2Btn()
            elif cmd == 'preset3':
                radio.preset3Btn()
            elif cmd == 'preset4':
                radio.preset4Btn()
            elif cmd == 'preset5':
                radio.preset5Btn()
            elif cmd.__len__() > 0 and cmd[0] == '#':
                comment = True
            else:
                print("Unknown command: \"" + cmd + "\"")
                print("Valid commands are: quit power volumeUp volumeDown mute amfm")
                print("                    tuneUp tuneDown seekUp seekDown set")
                print("                    preset1 preset2 preset3 preset4 preset5")

textui = TextUICarRadio()
TextUICarRadio.main()
