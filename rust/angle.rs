fn angle(vector: (f64, f64)) -> f64 {
    let pi = 3.1415926535;
    match vector {
        (0.0, y) if y < 0.0 => 1.5 * pi,
        (0.0, _) => 0.5 * pi,
        (x, y) => (y / x).atan()
    }
}

fn main() {
    // should be pi/4~=0.785398
    println!("angle of (1,1) is {}", angle((1.0,1.0)));
}
