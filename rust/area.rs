#![feature(struct_variant)]
use std::f64;
struct Point {
    x: f64,
    y: f64
}
enum Shape {
    Circle { center: Point, radius: f64 },
    Rectangle { top_left: Point, bottom_right: Point }
}
fn square(x: f64) -> f64 {
    return x * x;
}
fn area(sh: Shape) -> f64 {
    match sh {
        Circle { radius: radius, .. } => f64::consts::PI * square(radius),
        Rectangle { top_left: top_left, bottom_right: bottom_right } => { (bottom_right.x - top_left.x) * (top_left.y - bottom_right.y)
        }
    }
}

fn main() {
    println!("Area of circle with radius 3 = {}", area(Circle{center:Point { x: 0.0, y: 0.0 }, radius:10.0}));
}
