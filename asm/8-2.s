.HW:
    .string "Hello world\n"
    .text
    .globl main
    .type  main, @function
main:
    pushq %rbp
    movq  %rsp, %rbp
    movl  $13, %edx
    movl  $.HW, %esi
    movl  $1, %edi
    call  write
    movl  $0, %eax
    popq  %rbp
    ret
