.NEWSTR:
    .string "Hello World!\n"
    .text
    .globl main
    .type  main, @function
main:
    pushq %rbp
    movq  %rsp, %rbp
    movl  $'h', %edi
    movl  $0, %eax
    call  printf
    popq  %rbp
    ret
