.STR:
    .string "babe\n"
    .text
    .globl main
    .type  main, @function
main:
    pushq %rbp
    movq  %rsp, %rbp
    movl  $.STR, %edi
    movl  $STDOUT, %eax
    call  write
    popq  %rbp
    ret
