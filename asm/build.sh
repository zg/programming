#!/bin/sh
die(){
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 1 ] || die "usage: $0 <filename>"

as -gstabs -o $1.o $1.s && gcc -o $1 $1.o && chmod +x $1 && ./$1 && rm $1.o
