    .text
    .globl main
    .type  main, @function
main:
    pushq %rbp
    movq  %rsp, %rbp
    pushq %rbx
    pushq %r12
    pushq %r13
    pushq %r14
    pushq %r15
    movb  $0x12, %bl
    movw  $0xabcd, %r12w
    movl  $0x1234abcd, %r13d
    movq  $0xdcba, %r14
    movq  $0x9876, %r15
    popq  %r15
    popq  %r14
    popq  %r13
    popq  %r12
    popq  %rbx
    movl  $0, %eax
    popq  %rbp
    ret
