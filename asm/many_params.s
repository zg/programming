	.file	"test.c"
	.text
	.globl	f
	.type	f, @function
f:
	pushq	%rbp
	movq	%rsp, %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	%edx, -12(%rbp)
	movl	%ecx, -16(%rbp)
	movl	%r8d, -20(%rbp)
	movl	%r9d, -24(%rbp)
	movl	-8(%rbp), %eax
	movl	-4(%rbp), %edx
	addl	%eax, %edx
	movl	-12(%rbp), %eax
	addl	%eax, %edx
	movl	-16(%rbp), %eax
	addl	%eax, %edx
	movl	-20(%rbp), %eax
	addl	%eax, %edx
	movl	-24(%rbp), %eax
	addl	%eax, %edx
	movl	16(%rbp), %eax
	addl	%eax, %edx
	movl	24(%rbp), %eax
	addl	%eax, %edx
	movl	32(%rbp), %eax
	addl	%eax, %edx
	movl	40(%rbp), %eax
	addl	%eax, %edx
	movl	48(%rbp), %eax
	addl	%eax, %edx
	movl	56(%rbp), %eax
	addl	%eax, %edx
	movl	64(%rbp), %eax
	addl	%eax, %edx
	movl	72(%rbp), %eax
	addl	%eax, %edx
	movl	80(%rbp), %eax
	addl	%eax, %edx
	movl	88(%rbp), %eax
	addl	%eax, %edx
	movl	96(%rbp), %eax
	addl	%eax, %edx
	movl	104(%rbp), %eax
	addl	%eax, %edx
	movl	112(%rbp), %eax
	addl	%eax, %edx
	movl	120(%rbp), %eax
	addl	%eax, %edx
	movl	128(%rbp), %eax
	addl	%eax, %edx
	movl	136(%rbp), %eax
	addl	%eax, %edx
	movl	144(%rbp), %eax
	addl	%eax, %edx
	movl	152(%rbp), %eax
	addl	%eax, %edx
	movl	160(%rbp), %eax
	addl	%eax, %edx
	movl	168(%rbp), %eax
	addl	%edx, %eax
	popq	%rbp
	ret
	.size	f, .-f
	.globl	main
	.type	main, @function
main:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$160, %rsp
	movl	$26, 152(%rsp)
	movl	$25, 144(%rsp)
	movl	$24, 136(%rsp)
	movl	$23, 128(%rsp)
	movl	$22, 120(%rsp)
	movl	$21, 112(%rsp)
	movl	$20, 104(%rsp)
	movl	$19, 96(%rsp)
	movl	$18, 88(%rsp)
	movl	$17, 80(%rsp)
	movl	$16, 72(%rsp)
	movl	$15, 64(%rsp)
	movl	$14, 56(%rsp)
	movl	$13, 48(%rsp)
	movl	$12, 40(%rsp)
	movl	$11, 32(%rsp)
	movl	$10, 24(%rsp)
	movl	$9, 16(%rsp)
	movl	$8, 8(%rsp)
	movl	$7, (%rsp)
	movl	$6, %r9d
	movl	$5, %r8d
	movl	$4, %ecx
	movl	$3, %edx
	movl	$2, %esi
	movl	$1, %edi
	call	f
	leave
	ret
	.size	main, .-main
	.ident	"GCC: (GNU) 4.8.2 20140206 (prerelease)"
	.section	.note.GNU-stack,"",@progbits
