	.file	"stack.c"
	.comm	stack,2000,32
	.globl	stack_p
	.data
	.align 8
	.type	stack_p, @object
	.size	stack_p, 8
stack_p:
	.quad	stack+2000
	.text
	.globl	push
	.type	push, @function
push:
	pushq	%rbp
	movq	%rsp, %rbp
	movl	%edi, -4(%rbp)
	movq	stack_p(%rip), %rax
	subq	$4, %rax
	movq	%rax, stack_p(%rip)
	movq	stack_p(%rip), %rax
	movl	-4(%rbp), %edx
	movl	%edx, (%rax)
	popq	%rbp
	ret
	.size	push, .-push
	.globl	pop
	.type	pop, @function
pop:
	pushq	%rbp
	movq	%rsp, %rbp
	movq	%rdi, -8(%rbp)
	movq	stack_p(%rip), %rax
	leaq	4(%rax), %rdx
	movq	%rdx, stack_p(%rip)
	movl	(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, (%rax)
	popq	%rbp
	ret
	.size	pop, .-pop
	.section	.rodata
	.align 8
.LC0:
	.string	"Start with the stack pointer at %p\n"
.LC1:
	.string	"x = %i, y = %i, z = %i\n"
.LC2:
	.string	"push x\npush y\npush z"
	.align 8
.LC3:
	.string	"Now the stack pointer is at %p\n"
.LC4:
	.string	"Change x, y, and z:"
.LC5:
	.string	"x = %i, y = %i, and z = %i\n"
.LC6:
	.string	"pop z\npop y\npop x"
	.align 8
.LC7:
	.string	"And we end with the stack pointer at %p\n"
	.text
	.globl	main
	.type	main, @function
main:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$16, %rsp
	movl	$12, -4(%rbp)
	movl	$34, -8(%rbp)
	movl	$56, -12(%rbp)
	movq	stack_p(%rip), %rax
	movq	%rax, %rsi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	movl	-12(%rbp), %ecx
	movl	-8(%rbp), %edx
	movl	-4(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	movl	-4(%rbp), %eax
	movl	%eax, %edi
	call	push
	movl	-8(%rbp), %eax
	movl	%eax, %edi
	call	push
	movl	-12(%rbp), %eax
	movl	%eax, %edi
	call	push
	movl	$100, -4(%rbp)
	movl	$200, -8(%rbp)
	movl	$300, -12(%rbp)
	movl	$.LC2, %edi
	call	puts
	movq	stack_p(%rip), %rax
	movq	%rax, %rsi
	movl	$.LC3, %edi
	movl	$0, %eax
	call	printf
	movl	$.LC4, %edi
	call	puts
	movl	-12(%rbp), %ecx
	movl	-8(%rbp), %edx
	movl	-4(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC5, %edi
	movl	$0, %eax
	call	printf
	leaq	-12(%rbp), %rax
	movq	%rax, %rdi
	call	pop
	leaq	-8(%rbp), %rax
	movq	%rax, %rdi
	call	pop
	leaq	-4(%rbp), %rax
	movq	%rax, %rdi
	call	pop
	movl	$.LC6, %edi
	call	puts
	movq	stack_p(%rip), %rax
	movq	%rax, %rsi
	movl	$.LC7, %edi
	movl	$0, %eax
	call	printf
	movl	-12(%rbp), %ecx
	movl	-8(%rbp), %edx
	movl	-4(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %eax
	leave
	ret
	.size	main, .-main
	.ident	"GCC: (GNU) 4.8.2 20140206 (prerelease)"
	.section	.note.GNU-stack,"",@progbits
