	.file	"fib_bad.c"
	.section	.rodata
.LC0:
	.string	"%d\n"
	.text
	.globl	main
	.type	main, @function
main:
	pushq	%rbp
	movq	%rsp, %rbp
	movl	$10, %edi
	movl	$0, %eax
	call	fib
	movl	%eax, %esi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	popq	%rbp
	ret
fib:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%rbx
	subq	$24, %rsp
	movl	%edi, -20(%rbp)
	cmpl	$0, -20(%rbp)
	jne	.L3
	movl	$0, %eax
	jmp	.L4
.L3:
	cmpl	$1, -20(%rbp)
	jne	.L5
	movl	$1, %eax
	jmp	.L4
.L5:
	movl	-20(%rbp), %eax
	subl	$1, %eax
	movl	%eax, %edi
	call	fib
	movl	%eax, %ebx
	movl	-20(%rbp), %eax
	subl	$2, %eax
	movl	%eax, %edi
	call	fib
	addl	%ebx, %eax
.L4:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
