#include <stdio.h>

#define SIZE 500

int stack[SIZE];
int *stack_p = &stack[SIZE];

void push(int val){
    *(--stack_p) = val;
}

void pop(int *loc){
    *loc = *(stack_p++);
}

int main(void){
    int x = 12, y = 34, z = 56;
    printf("Start with the stack pointer at %p\n",(void *)stack_p);
    printf("x = %i, y = %i, z = %i\n",x,y,z);
    push(x);
    push(y);
    push(z);
    x = 100;
    y = 200;
    z = 300;
    printf("push x\npush y\npush z\n");
    printf("Now the stack pointer is at %p\n",(void *)stack_p);
    printf("Change x, y, and z:\n");
    printf("x = %i, y = %i, and z = %i\n",x,y,z);
    pop(&z);
    pop(&y);
    pop(&x);
    printf("pop z\npop y\npop x\n");
    printf("And we end with the stack pointer at %p\n",(void *)stack_p);
    printf("x = %i, y = %i, z = %i\n",x,y,z);
    return 0;
}
