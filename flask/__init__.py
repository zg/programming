import os
from flask import Flask
from flask import render_template
from flask import send_from_directory
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/css/style.css')
def getStylesheet():
    return send_from_directory(os.path.join(app.root_path,'css'),'style.css',mimetype='text/css')

@app.route("/user/<username>")
def show_user_profile(username):
    return "User %s" % username

@app.route("/post/<int:post_id>")
def show_post(post_id):
    return "Post %d" % post_id

@app.route('/favicon.ico')
def getFavicon():
    return send_from_directory(os.path.join(app.root_path,'images'),'favicon.ico',mimetype='image/vnd.microsoft.icon')

if __name__ == "__main__":
    app.run(host='0.0.0.0')
