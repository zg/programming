/*
 * intAndFloat.c
 * Using printf to display an integer and a float.
 * zg - 8 March 2014
 */
#include <stdio.h>

int main(void)
{
    int anInt = 19088743;
    float aFloat = 19088.743;

    printf("The integer is %i and the float is %f\n", anInt, aFloat);

    return 0;
}
