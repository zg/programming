#include <stdio.h>

#define SIZE 256

main()
{
    int c, index, index_, high;
    int chars[SIZE];
    high = index = index_ = 0;
    for(index = 0; index < SIZE; ++index)
        chars[index] = 0;
    while((c = getchar()) != EOF)
        ++chars[c];
    for(index = 0; index < SIZE; ++index)
        if(chars[index] > high)
            high = chars[index];
    for(index = high; index >= 1; --index)
    {
        printf("%4d |",index);
        for(index_ = 0; index_ < SIZE; ++index_)
            if(chars[index_] > 0)
                if(chars[index_] >= index)
                    if(index_ == '\n' || index_ == '\t' || index_ == '\b')
                        printf("* ");
                    else
                        printf("*");
                else
                    printf(" ");
        printf("\n");
    }
    printf("     +");
    for(index = 0; index < SIZE; ++index)
        if(chars[index] > 0)
            if(index == '\n' || index == '\t' || index == '\b')
                printf("--");
            else
    printf("-");
    printf("\n ANSI ");
    for(index = 0; index < SIZE; ++index)
        if(chars[index] > 0)
            if(index == '\n')
                printf("\\n");
            else if(index == '\t')
                printf("\\t");
            else if(index == '\b')
                printf("\\b");
            else
                putchar(index);
}
