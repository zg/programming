#include <stdio.h>

#define K 200

int main(void){
    int p[K], p_idx = -1, i, j, flag;
    for(i = 2; i <= K; i++){
        flag = 1;
        for(j = 0; j < p_idx; j++){
            if(i % p[j] == 0){
                flag = 0;
            }
        }
        if(flag == 1){
            p[++p_idx] = i;
        }
    }
    for(i = 0; i <= p_idx; i++)
        printf("%d %d\n",i,p[i]);
}
