#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char *argv[]){
    int i, offset, dec;

    if(argc == 2){
        dec = atoi(argv[1]);
    } else {
        printf("> ");
        scanf("%d",&dec);
    }
    int size = floor(log2(dec)) + 1;
    int bins[size];
    printf("dec: %d\n",dec);
    if(dec < 0){
        printf("Decimal too low.\n");
        return -1;
    }
    for(i = 0; i < size; i++){
        bins[i] = 0;
    }
    offset = 1;
    while(0 < dec){
        if(dec % 2 == 0){ // even
            bins[size-offset] = 0;
            dec /= 2;
        } else { // odd
            bins[size-offset] = 1;
            dec--;
            dec /= 2;
        }
        offset++;
    }
    printf("bin: ");
    for(i = 0; i < size; i++){
        printf("%d",bins[i]);
    }
    printf("\n");
    return 0;
}
