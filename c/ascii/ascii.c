#include <stdio.h>

int isprint(int c);

int main(void){
    int i;
    for(i = 20; i <= 126; i++)
        if(isprint(i))
            printf("%d = %c\n",i,i);
    return 0;
}
