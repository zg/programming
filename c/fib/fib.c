//only accurate up to K=71

#include <stdio.h>
#include <math.h>

#define K 71

double fib(int n);

int main(void){
    printf("%f\n",fib(K));
}

double fib(int n){
    return ((pow((1+sqrt(5))/2,n)-pow((1-(1+sqrt(5))/2),n))/sqrt(5));
}
