//infeasible

#include <stdio.h>

#define K 10

int main(void){
    printf("%d\n",fib(K));
}

int fib(int n){
    if(n == 0){
        return 0;
    } else if(n == 1){
        return 1;
    } else {
        return fib(n - 1) + fib(n - 2);
    }
}
