#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define SIZE 32

int main(int argc, char *argv[]){
    int i, l = 0, t = 0;
    char bins[SIZE];
    for(i = 0; i < SIZE; i++)
        bins[i] = 0;
    printf("> ");
    fgets(bins,SIZE + 1,stdin);
    for(i = 0; i < SIZE; i++){
        if(bins[i] == '0' || bins[i] == '1'){
            l++;
            continue;
        } else if(bins[i] == '\n' || bins[i] == '\0'){
            break;
        } else {
            printf("Invalid format.\n");
            return -1;
        }
    }
    printf("bin: ");
    for(i = 0; i < l; i++)
        printf("%c",bins[i]);
    printf("\n");
    printf("dec: ");
    for(i = l - 1; i >= 0; i--)
        t = (t << 1) ^ (bins[i] - '0');
    printf("%d\n",t);
    return 0;
}
