#include <stdio.h>
#include "funcs.h"

unsigned int int_size(){
    int size = 0;
    for(unsigned int i = ~0u; i > 0; i >>= 1){
        size++;
    }
    return size;
}

unsigned int get_mask(int count){
    unsigned int mask = 1;
    for(int i = 0; i < count; i++)
        mask |= 1 << i;
    return mask;
}

unsigned int bit_get(unsigned int data, unsigned int n){
    return (data >> (int_size() - n - 1)) & 1;
}

unsigned int bit_set(unsigned int data, unsigned int n){
    return data | (1 << (int_size() - n - 1));
}

unsigned int bit_width(unsigned int data){
    int width = int_size();
    for(; width > 0; width--)
        if((data & (1 << width)) != 0)
            break;
    return width + 1;
}

void print_data(unsigned int data){
    const unsigned int size = int_size();
    printf("%08X = ",data);
    for(int i = 0; i < size; i++){
        if(i % 4 == 0)
            putchar(' ');
        printf("%X",bit_get(data,i));
    }
    putchar('\n');
}

unsigned int bitpat_search(unsigned int source, unsigned int pattern,
        unsigned int n){
    const int right = int_size() - n;
    unsigned int mask = get_mask(n);
    for(int i = 0; i < right; i++)
        if(((source & (mask << (right - i))) >> (right - i) ^ pattern) == 0)
            return i - bit_width(source);
    return -1;
}

unsigned int bitpat_get(unsigned int data, int start, int count){
    if(start < 0 || count < 0 || int_size() <= start || int_size() <= count || bit_width(data) != count)
        return -1;
    unsigned int mask = get_mask(count);
    mask <<= int_size() - start - count;
    return (data & mask) >> (int_size() - start - count);
}

void bitpat_set(unsigned int *data, unsigned int replace, int start, int count){
    if(start < 0 || count < 0 || int_size() <= start || int_size() <= count || bit_width(replace) != count)
        return;
    unsigned int mask = get_mask(count);
    *data = ((*data | (mask << (int_size() - start - count))) & ~(mask << (int_size() - start - count))) | (replace << (int_size() - start - count));
}
