// Exercise 12.8 from Programming in C ISBN 0672326663

#include <stdio.h>
#include "funcs.h"

int main(void){
    const unsigned int data = 0x260ce1f2;
    const unsigned int *part = &data;
    print_data(*part);
    bitpat_set(part,0x55fu,0,12);
    print_data(*part);
    return 0;
}
