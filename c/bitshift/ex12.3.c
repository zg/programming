// Exercise 12.3 from Programming in C ISBN 0672326663

#include <stdio.h>

unsigned int int_size(unsigned int i){
    const unsigned int size = 0;
    while(i > 0){
        i >>= 1;
        size++;
    }
    return size;
}

int main(void){
    printf("%d\n",int_size(~0u));
    return 0;
}
