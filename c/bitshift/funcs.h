unsigned int int_size();
unsigned int bit_width(unsigned int data);
unsigned int bit_get(unsigned int, unsigned int);
unsigned int bit_set(unsigned int, unsigned int);
unsigned int bitpat_search(unsigned int, unsigned int, unsigned int);
unsigned int bitpat_get(unsigned int, int, int);
void bitpat_set(unsigned int *, unsigned int, int, int);
void print_data(unsigned int);
