// Program 12.4 from Programming in C ISBN 0672326663

#include <stdio.h>
#include "funcs.h"

unsigned int rotate(unsigned int value, int n){
    const unsigned int intsize = int_size();
    const unsigned int result, bits;

    if(n > 0)
        n = n % intsize;
    else
        n = -(-n % intsize);

    if(n == 0)
        result = value;
    else if(n > 0){
        bits = value >> (intsize - n);
        result = value << n | bits;
    } else {
        n = -n;
        bits = value << (intsize - n);
        result = value >> n | bits;
    }

    return result;
}

int main(void){
    const unsigned int w1 = 0xabcdef00u, w2 = 0xffff1122u;
    //moving this before main
    //unsigned int rotate (unsigned int value, int n);

    printf("%x\n", rotate(w1, 8));
    printf("%x\n", rotate(w1, -16));
    printf("%x\n", rotate(w2, 4));
    printf("%x\n", rotate(w2, -2));
    printf("%x\n", rotate(w1, 0));
    printf("%x\n", rotate(w1, 44));

    return 0;
}
