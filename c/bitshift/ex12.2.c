// Exercise 12.2 from Programming in C ISBN 0672326663

#include <stdio.h>

int main(void){
    if(1u >> 1 == 0)
        printf("arithmetic");
    else
        printf("logical");
    printf(" right shift\n");
    return 0;
}
