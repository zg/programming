// Exercise 12.5 from Programming in C ISBN 0672326663

#include <stdio.h>
#include "funcs.h"

int main(void){
    const unsigned int data = 0x0f0f;
    // 0x7 = 0111
    printf("%X = ",data);
    for(int i = 0; i < int_size(); i++)
        printf("%X",bit_get(data,i));
    putchar('\n');
    for(int i = 0; i < int_size(); i++){
        unsigned int new_data = bit_set(data,i);
        printf("set %X[%2d] = ",data,i);
        for(int j = 0; j < int_size(); j++)
            printf("%X",bit_get(new_data,j));
        putchar('\n');
    }
}
